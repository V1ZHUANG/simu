## Technical Reference - ACMI flight recordings 2.1




### Introduction
[Source](https://www.tacview.net/documentation/acmi/en/)

With Tacview 1.5 a new universal public file format has been introduced. The goal was to overcome the complexity of the previous format while making it much more powerful at the same time.

Like its predecessor, the new file format is written in plain [UTF-8](https://en.wikipedia.org/wiki/UTF-8) text. That way, it is possible to easily export flight data from the simplest programming language. Its syntax is very easy to read by humans and, with the debug log introduced in Tacview 1.4.3, it is now very easy to diagnose any exporter issues. This new format is so simple that it could even be written by hand if the amount of data was not astronomic!

Despite its simplicity, the new format offers a very powerful way to set and change – in real-time – any property of any object on the battlefield. For instance, it is now possible to change the coalition, the color, even the type of an object, on the fly! In the same way, you can easily assign and change global properties, like the weather for example.

It is important to note that data which are not yet supported by Tacview are preserved and visible in the raw telemetry window. If you think that an important data should be natively supported and displayed by Tacview, feel free to [contact us](https://www.tacview.net/support/featurerequest).

Tacview 1.5引入了一种新的通用公共文件格式。目的是要克服以前格式的复杂性，同时使其功能更强大。

和之前一样，这个新的文件格式仍是以纯[UTF-8]文本编写。这样，可以很容易地用最简单的编程语言来导出飞行数据。 Tacview 1.4.3中引入了调试日志，它的语法很容易被阅读，现在很容易诊断出任何输出问题。这种新格式非常简单，如果数据量不是天文数字的话甚至都可以用手工编写！

尽管格式简单，但它提供了一种非常强大的方式来实时设置和更改战场上任何物体的任何属性。例如，现在可以即时更改联盟、颜色甚至对象的类型！同样，您可以轻松设置和更改全局属性，例如天气。

重要的是要注意，Tacview尚不支持的数据将保留并在原始遥测窗口中可见。如果您认为重要数据应由Tacview本地支持并显示，请随时与我们联系。

### ACMI 2.1 File Format 101

Without further ado, let's start with the simplest file possible:

让我们马上从最简单的文件开始：

```
FileType=text/acmi/tacview
FileVersion=2.1
```

These are the only two mandatory lines you must put first in any ACMI file. This header tells Tacview which format to expect. Any following data is optional.

Let's be rational: Even if Tacview will gracefully load this empty file, we need a bit more data to make it useful! Here is a file which makes more sense:

首先任何ACMI文件中必须具有上述两行强制性的文件头。 这个文件头告诉Tacview期望使用哪种格式。 后面的任何数据都是可选的。

当然从道理上说，即使Tacview会正常加载这个空文件，但我们也需要更多数据才能使其有用！ 这是一个更有意义的文件：

```
FileType=text/acmi/tacview
FileVersion=2.1
0,ReferenceTime=2011-06-02T05:00:00Z
#47.13
3000102,T=41.6251307|41.5910417|2000.14,Name=C172
```

To better understand this structure, we need to know that – apart from its header – each line of the file can be either:

- The sharp sign `#` introducing a new time frame in seconds relative to `ReferenceTime`
- An object id (in this example `0` and `3000102`) followed by as many properties as you want separated by commas ,. Each property will be assigned a new value using the equal sign `=`.
- The third possibility – not shown here – is a line which starts with the minus sign `-` followed by the id of an object we want to remove from the battlefield (could be destroyed or simply out of recording range).

Let's see in detail each line syntax:

为了更好地理解这种结构，我们需要知道，除了其头两行的文件头之外，文件的每一行都可以是：

- 符号`＃`后的数字表示相对于`ReferenceTime`以秒为单位的一个新的时间帧
- 对象ID（在此示例中为`0`和`3000102`），后跟任意多个用逗号分隔的属性，并使用等号 `=`为每个属性赋予一个新值。
- 第三种可能性（此处未显示 ），以减号 `-` 开头的行，后面跟着的是我们要从战场中删除的对象ID（可能被摧毁或超出记录范围）。

下面让我们详细了解每一行的语法：

```
0,ReferenceTime=2011-06-02T05:00:00Z
```

This line assigns the value `2011-06-02T05:00:00Z` to the property `ReferenceTime` of the global object always designated by its id zero 0. In other words: This line defines the base/reference time used for the whole flight recording. To understand better what this means, let's have a look at the following line:

此行将值 `2011-06-02T05:00:00Z` 赋予给ID=0的始终是全局对象“零”的`ReferenceTime`属性。换句话说：此行定义用于确定整个飞行记录的基准/参考时间。 为了更好地理解这个含义，让我们看一下接下来这行：

```
#47.13
```

This line defines a time-frame in seconds relative to `ReferenceTime`. In that case, this means that the following events or properties happened at `ReferenceTime + 47.13 seconds` ⇒ `2011-06-02T05:00:00Z + 47.13` ⇒` 2011-06-02T05:00:47.13Z`

Now let's see the following line:

该行定义了一个相对于`ReferenceTime`的时间帧（以秒为单位）。 在这种情况下，这意味着以下事件或属性发生在`ReferenceTime + 47.13秒`⇒`2011-06-02T05:00:00Z + 47.13`⇒`2011-06-02T05:00:47.13Z`

再看下一行：

```
3000102,T=41.6251307|41.5910417|2000.14,Name=C172
```

This line defines two properties for the object `3000102`. To save space, Object ids are expressed in [hexadecimal](https://en.wikipedia.org/wiki/Hexadecimal) without any prefix or leading zeros.

The first property `T` (which stands for Transform) is a special property used to define the object coordinates in space. We will see later which syntaxes are supported for `T`. For now, let's just focus on this case which is:` T = Longitude | Latitude | Altitude`.

Notice that `Latitude` and `Longitude` are expressed in degrees. Positive values are toward the north and east directions. Since the whole file is always in the metric system, the altitude is expressed in meters [MSL](https://en.wikipedia.org/wiki/Metres_above_sea_level) (above sea level, also known as ASL in some countries).

The following property `Name` obviously defines the object name `C172` which is a short way of designating a [Cessna 172](https://en.wikipedia.org/wiki/Cessna_172) aircraft.

Now that you know all the basics to create a flight recording, let's move our new aircraft a bit further to the east. To do so, we can simply add another frame to our file:

该行定义了对象3000102的两个属性。为节省空间，对象ID以十六进制表示，没有任何前缀或前导零。

第一个属性`T`（代表Transform）是一种特殊的属性，用于定义空间中的对象坐标。稍后我们将了解`T`支持哪些语法。现在，我们仅关注这种情况：`T =经度|纬度|高度`。

请注意，纬度和经度以度表示。正值朝北和朝东。由于整个文件始终采用公制，因此高度以米[MSL]表示（海平面以上，在某些国家中也称为ASL）。

后面的属性名称显然定义了对象名称`Name`是`C172`，这是指定Cessna 172飞机的一种缩写。

创建飞行记录的所有基本知识就是这些了，然后我们将这个飞机向东移动一点。为此，我们可以简单地在文件中添加另一帧：

```
#49
3000102,T=41.626||
```

As you can see, we have defined a new longitude value `41.626` for our aircraft at the time frame` 2011-06-02T05:00:49Z`

You may have noticed that we don't need to specify – again – the aircraft name, simply because it has not changed since the last time! Another difference with the previous record is that we have omitted the latitude and altitude parameters because they did not change either. This helps to save a lot of space when generating data for long flights. While aircraft are usually quite mobile, this optimization is especially relevant for ground objects which can stay still or move just a little bit time to time...

如您所见，我们在`2011-06-02T05:00:49Z`的时间帧为飞机定义了新的经度值`41.626`。

可能您已经注意到，我们不需要再次指定飞机名称，只是因为自上条记录以来就没有改变过！ 与上一条记录的另一个区别是我们省略了纬度和海拔参数，因为它们也没有改变。 在生成长途飞行数据时，这有助于节省大量空间。 虽然飞机通常机动性很强，但这种优化特别适用于可以静止不动或时不时移动的地面物体。

### Detailed File Specifications

Now that you are starting to understand better how ACMI files are structured, let's review together the requirements and some tips related to the file format in general:

为更好地了解ACMI文件的结构，下面让我们一起总结一下文件的要求和与文件格式有关的一些技巧：

#### Requirements

- Text data must be written in UTF-8. That way, all languages are supported for text properties.
- All data are expressed in the metric system, using meters, meters per second for speed, degrees for angles, [UTC time](https://en.wikipedia.org/wiki/Coordinated_Universal_Time) and so on.
- Object ids are expressed using 64-bit hexadecimal numbers (without prefix or leading zeros to save space)
- The object `0` is used to define global properties (like` ReferenceTime` or `Briefing`)
- When you want to assign a text property which contains a comma , you must put the escape character `\` before it so it is not interpreted by Tacview as the end of your string.

- 文本数据必须以UTF-8格式写入。 从而文本属性就支持所有语言。
- 所有数据都以公制表示，以米为单位，如用米每秒数表示速度、用度表示角度、UTC时间等。
- 对象ID使用64位十六进制数字表示（不带前缀或前导零以节省空间）
- 对象`0`用于定义全局属性（如`ReferenceTime`或`Briefing`）
- 当要赋予一个包含逗号的文本属性时，必须在其前面加上转义字符 `\`，以便Tacview不会将其解释为字符串的结尾。

```
Briefing=Here is a text value\, which contains an escaped comma in it!
```

#### Tips

- To save space, it is strongly suggested to end lines with the LF `\n` character only.
- It is cleaner to prefix text data with the UTF-8 [BOM](https://en.wikipedia.org/wiki/Byte_order_mark) header.
- The whole of the text data can be wrapped in a zip or 7z container to save bandwidth or disk space.
- Data can be presented out-of-order. Tacview will do its best to reorder it in memory.

- 为了节省空间，强烈建议仅使用LF `\n`字符结束行。
- 使用UTF-8 字节顺序标记（BOM）标头为文本数据添加前缀码会更干净。
- 整个文本数据可以包装在zip或7z容器中，以节省带宽或磁盘空间。
- 数据可以乱序显示， Tacview会在内存中重新排序。

### Object Coordinates

Now let's have a closer look at the different notations for object coordinates. To optimize the file size, Tacview offers four different notations.

Here are two examples: When exporting a bullet coordinate, we do not need any data about its rotation angles. The opposite example would be an aircraft in a flight simulator running in a flat world like Falcon 4.0: In that case, to get accurate replay, we should export the native position of the aircraft in the flat world, its rotation, and its coordinates in a spherical world. That way the aircraft will not only be properly displayed in Tacview's spherical world, but telemetry calculation will be done in the object's native coordinate system so the numbers visible on screen will match the ones you can see in the original flight simulator.

现在，让我们仔细看看对象坐标的不同表示法。 为了优化文件大小，Tacview提供了四种不同的表示法。

这是两个示例：导出一个子弹坐标时，我们不需要有关其旋转角度的任何数据。 相反的例子是飞行模拟器中的飞机在像Falcon 4.0这样的平面世界中飞行：在这种情况下，为了获得准确的重放，我们需要导出飞机在平面世界中的原始位置、旋转以及在球形的世界的坐标 。 这样，飞机不仅可以在Tacview的球形世界中正确显示，而且遥测计算也可在对象的原始坐标系中完成，因此屏幕上见到的数字将与在原始飞行模拟器中看到的数字匹配。

| Object Position Syntax                                       | Purpose                                                      |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| `T = Longitude \| Latitude \| Altitude`                      | Simple objects in a spherical world (typically minor objects like bullets). Can also be relevant for low-end data source like GPX files without rotation information.球形世界中的简单对象（通常是子弹之类的次要对象）。 也可以与没有旋转信息的低端数据源（如GPX文件）相关。 |
| `T = Longitude \| Latitude \| Altitude \| U \| V`            | Simple objects from a flat world. U & V represent the native x and y. Do not forget to express them in meters even if the original coordinates are in feet for example. Altitude is not repeated because it is the same for both native and spherical worlds.来自平面世界坐标的简单对象。 U＆V代表本机x和y。 即使原始坐标以英尺为单位，也不要忘记换算为以米为单位。 海拔高度不重复省略掉了，因为原始世界坐标和球形世界坐标都一样。 |
| `T = Longitude \| Latitude \| Altitude \| Roll \| Pitch \| Yaw` | Complex objects in a spherical world. Roll is positive when rolling the aircraft to the right. Pitch is positive when taking-off. Yaw is clockwise relative to the true north.球形世界中的复杂对象。 飞机向右滚转时，滚转为正。 起飞时俯仰为正。 偏航角相对于正北的顺时针方向夹角。 |
| `T = Longitude \| Latitude \| Altitude \| Roll \| Pitch \| Yaw \| U \| V \| Heading` | Complex object from a flat world. Same as before. Heading is the yaw relative to the true north of the flat world. It is required because the native world north usually does not match spherical world north because of projection errors.来自平面世界的复杂对象。 和之前一样。 航向（Heading）是相对于平面世界正北的偏航角。 之所以需要这样做，是因为由于投影误差，原始世界的北通常与球形世界的北并不一致。 |

Remember that you can omit the components which did not change since the last time. This will save a lot of space.

If some of the data is missing (for example object rotation), Tacview will do its best to emulate it in order to give a nice replay. Independently from optimization, you should keep the same data notation for each object during the object life. If at one point you use a different notation, Tacview will do its best to promote the object to a more complex one. However – because of the initial lack of data – the final result may not be the expected one.

请记住，您可以省略自上次以来未更改的组件。 这样可以节省很多空间。

如果某些数据丢失（例如，对象旋转），Tacview将会去尽量模拟它，以提供良好的重放。 与优化无关，在对象生命周期中，应为每个对象保留相同的数据表示法。 如果在某一时刻使用了不同的表示法，Tacview将会把对象升级为更复杂的表示法。 但是，由于最初缺乏数据，最终结果可能难以预料。

### Global Properties

We already saw that one of the most important global properties is the `ReferenceTime`. Obviously, there are plenty of other meta-data you can inject in a flight recording to make your replay more detailed.

我们已经看到，最重要的全局属性之一是`ReferenceTime`。 显然，您可以在飞行记录中注入许多其他元数据，以使重放更加详细。

#### Text Properties

| Property Name   | Meaning                                                      |
| :-------------- | :----------------------------------------------------------- |
| `DataSource`    | Source simulator, control station or file format. 源模拟器，控制台或文件格式。<br>`DataSource=DCS 2.0.0.48763 `<br>`DataSource=GPX File` |
| `DataRecorder`  | Software or hardware used to record the data. 用于记录数据的软件或硬件。<br>`DataRecorder=Tacview 1.5 `<br>`DataRecorder=Falcon 4.0` |
| `ReferenceTime` | Base time (UTC) for the current mission. This time is combined with each frame offset (in seconds) to get the final absolute UTC time for each data sample. 当前任务的基准时间（UTC）。 该时间与每个帧偏移（以秒为单位）相结合，以获得每个数据样本最终的绝对UTC时间。<br>`ReferenceTime=2011-06-02T05:00:00Z` |
| `RecordingTime` | Recording (file) creation (UTC) time. 记录（文件）创建（UTC）时间。<br>`RecordingTime=2016-02-18T16:44:12Z` |
| `Author`        | Author or operator who has created this recording. 创建此记录的作者或操作员。<br>`Author=Lt. Cmdr. Rick 'Jester' Heatherly` |
| `Title`         | Mission/flight title or designation. 任务/飞行的标题或名称。<br>Title=Counter Attack |
| `Category`      | Category of the flight/mission. 飞行/任务的类别。<br>`Category=Close air support` |
| `Briefing`      | Free text containing the briefing of the flight/mission. 包含飞行/任务简介的自由文本。 <br>`Briefing=Destroy all SCUD launchers` |
| `Debriefing`    | Free text containing the debriefing. 包含任务报告的自由文本。<br>`Debriefing=Managed to stay ahead of the airplane.` |
| `Comments`      | Free comments about the flight. Do not forget to escape any end-of-line character you want to inject into the comments. 关于飞行的自由评论。 不要忘记转义任何要插入注释中的行尾字符。<br>`Comments=Part of the recording is missing because of technical difficulties.` |

#### Numeric Properties

| Property Name                                 | Unit | Meaning                                                      |
| :-------------------------------------------- | :--- | :----------------------------------------------------------- |
| `ReferenceLongitude`<br/>` ReferenceLatitude` | deg  | These properties are used to reduce the file size by centering coordinates around a median point. They will be added to each object Longitude and Latitude to get the final coordinates.这些属性用于通过将坐标以一个中间点为原点来减小文件大小。 它们会被加到每个对象的经度和纬度来求得最终坐标。 <br>`ReferenceLongitude=-129 `<br>`ReferenceLatitude=43` |













#### Events

Events can be used to inject any kind of text, bookmark and debug information into the flight recording. They are a bit special: They are declared like properties, but unlike properties, you can declare several events in the same frame without overriding the previous one.

Here is an example on how to inject events:

事件可用于将任何类型的文本、记号和调试信息注入飞行记录中。 它们有点特殊：它们像属性一样被声明，但是与属性不同，您可以在同一帧中声明多个事件而不会覆盖前一个事件。

这是有关如何注入事件的示例：





```
#8.62
0,Event=Message|3000100|Here is a generic event linked to the object 3000100
0,Event=Bookmark|Here is a bookmark to highlight a specific part of the mission!
#8.72
0,Event=Debug|Here is some debug text, visible only with the /Debug:on command line option
```

You may notice the structure of an event declaration:

```
Event = EventType | FirstObjectId | SecondObjectId | ... | EventText
```

For each event we must declare first the type of the event (e.g. `Bookmark`), optionally followed by ids of concerned objects. For example, when the user double click on the event, Tacview will use theses ids to automatically center the camera around associated objects. The last part is a mandatory text message. Even if it is possible to provide an empty text, it is suggested to provide a useful message to get the most out of your debriefings.

Here are the different kind of events currently supported by Tacview:

对于每个事件，我们必须首先声明事件的类型（例如：`标记`），然后可选地声明有关对象的ID。 例如，当用户双击事件时，Tacview将使用这些ID将摄像机自动围绕相关对象居中。 最后一部分是强制性的文本信息。 即使可以提供空白文本，也建议您提供有用的信息，以充分利用报告。

这是Tacview当前支持的不同类型的事件：



这是Tacview当前支持的不同类型的事件：

| Event Name  | Meaning                                                      |
| :---------- | :----------------------------------------------------------- |
| `Message`   | Generic event. 通用事件。<br/>`0,Event=Message\|705\|Maverick has violated ATC directives` |
| `Bookmark`  | Bookmarks are highlighted in the time line and in the event log. They are easy to spot and handy to highlight parts of the flight, like a bombing run, or when the trainee was in her final approach for landing. 记号在时间线和事件日志中突出显示。 它们很容易发现，并且易于突出飞行中的某些部分，例如轰炸或受训者最后着陆时。<br/>`0,Event=Bookmark\|Starting precautionary landing practice` |
| `Debug`     | Debug events are highlighted and easy to spot in the timeline and event log. Because they must be used for development purposes, they are displayed only when launching Tacview with the command line argument /Debug:on <br/>调试事件高亮显示，很容易在时间轴和事件日志中发现。 由于必须将它们用于开发目的，因此仅在使用命令行启动Tacview时带上参数/Debug:on时才会显示它们<br/>`0,Event=Debug\|327 active planes` |
| `LeftArea`  | This event is useful to specify when an aircraft (or any object) is cleanly removed from the battlefield (not destroyed). This prevents Tacview from generating a Destroyed event by error. <br/>此事件对于指定何时将飞机（或任何物体）从战场上完全地移走（不是被摧毁）非常有用。 这样可以防止Tacview错误生成Destroyed事件。<br/>`0,Event=LeftArea\|507\|` |
| `Destroyed` | When an object has been officially destroyed.<br/>当物体被正式摧毁时。<br/>`0,Event=Destroyed\|6A56\|` |
| `TakenOff`  | Because Tacview may not always properly auto-detect take-off events, it can be useful to manually inject this event in the flight recording. <br/>由于Tacview可能无法始终正确地自动检测起飞事件，因此在飞行记录中手动添加此事件可能很有用。<br/>`0,Event=TakenOff\|2723\|Col. Sinclair has taken off from Camarillo Airport` |
| `Landed`    | Because Tacview may not always properly auto-detect landing events, it can be useful to manually inject this event in the flight recording. <br/>由于Tacview可能无法始终正确地自动检测降落事件，因此在飞行记录中手动添加此事件可能很有用。<br/>`0,Event=Landed\|705\|Maverick has landed on the USS Ranger` |
| `Timeout`     | Mainly used for real-life training debriefing to specify when a weapon (typically a missile) reaches or misses its target. Tacview will report in the shot log as well as in the 3D view the result of the shot. Most parameters are optional. SourceId designates the object which has fired the weapon, while TargetId designates the target. Even if the displayed result may be in nautical miles, bullseye coordinates must be specified in meters. The target must be explicitly (manually) destroyed or disabled using the appropriate properties independently from this event. <br/>主要用于现实世界中的训练报告，以指定武器（通常是导弹）何时达到或错过其目标。 Tacview将在射击日志以及3D视图中报告射击结果。 大多数参数是可选的。 SourceId指定发射武器的对象，而TargetId为指定的射击目标。 即使显示的结果可能是海里，也必须以米为单位指定靶心坐标。 必须使用独立于此事件的适当属性来明确（手动）销毁或禁用目标。<br/>`0,Event=Timeout\|SourceId:507\|AmmoType:FOX2\|AmmoCount:1\|Bullseye:50/15000/2500`<br/>`\|TargetId:201\|IntendedTarget:Leader\|Outcome:Kill` |





### Object Properties

Since Tacview 1.5, it is possible to set and change any object property in real-time. Even if new properties may not always be visible in the 3D view, you can always have a look at the raw telemetry window to see what is the current value of each property for currently selected objects.

Tacview 1.7 has introduced a new object [database](https://www.tacview.net/documentation/database/en/) which enables you to predefine any of the object properties expect for `Type` and `Name`. For example, you can predefine the default `shape` of a `F-16C` in that database. If the `Shape` property value is not defined in the telemetry file, Tacview will use the value stored in the database and display your custom 3D model for the F-16C in the 3D view.

Learn how to update and extend Tacview database by reading the [dedicated documentation](https://www.tacview.net/documentation/database/en/).

从Tacview 1.5开始，可以实时设置和更改任何对象属性。 即使新属性可能并不总是在3D视图中可见，您也可以始终在原始遥测窗口中查看当前选定对象的每个属性的当前值。

Tacview 1.7引入了一个新的对象 database，可以预定义`Type`和`Name`所需的任何对象属性。 例如，您可以在该数据库中预定义F-16C的默认形状。 如果遥测文件中未定义Shape属性值，则Tacview将使用数据库中存储的值，并在3D视图中显示F-16C的自定义3D模型。

通过阅读专用文档了解如何更新和扩展Tacview数据库。

#### Text Properties

| Property Name  | Meaning                                                      |
| :------------- | :----------------------------------------------------------- |
| Name           | The object name should use the most common notation for each object. It is strongly recommended to use [ICAO](https://www.icao.int/publications/DOC8643/Pages/Search.aspx) or [NATO](https://en.wikipedia.org/wiki/NATO_reporting_name) names like: `C172` or `F/A-18C`. This will help Tacview to associate each object with the corresponding entry in its database. `Type` and `Name` are the only properties which *CANNOT* be predefined in Tacview [database](https://www.tacview.net/documentation/database/en/). <br/>对象名称应为每个对象使用最通用的符号。 强烈建议使用ICAO或北约名称，例如：C172或F A-18C。 这将有助于Tacview将每个对象与其数据库中的相应条目相关联。 只有类型Type和名称Name是不能在Tacview数据库中预定义的属性。<br/>`Name=F-16C-52` |
| Type           | Object types are built using tags. This makes object management much more powerful and transparent than with the previous exclusive types. (see below for the list of supported types). Type and Name are the only properties which *CANNOT* be predefined in Tacview [database](https://www.tacview.net/documentation/database/en/). <br/>对象类型是使用标签构建的。 与以前的独占类型相比，这使对象管理更加强大和透明。 （请参阅下面的受支持类型列表）。只有类型`Type`和名称`Name`是不能在Tacview数据库中预定义的属性。<br/>`Type=Air+FixedWing` |
| AdditionalType | Any tags defined here will be added to the current object `Type`. This is useful to force an object type which has not been defined explicitly in the telemetry data. For example, you can use this property to automatically set the `FixedWing` tag for a `Cessna 172` telemetry data which come from a Garmin csv file (which usually does not contain any type declaration). For obvious reasons, this property must be used only in Tacview database, *NOT* in telemetry files. <br/>此处定义的所有标签都将添加到当前对象类型中。 这对强制在遥测数据中未明确定义的对象类型很有用。 例如，您可以使用此属性为来自Garmin csv文件（通常不包含任何类型声明）的`Cessna 172`遥测数据自动设置`FixedWing`标签。 出于明显的原因，此属性只能在Tacview数据库中使用，而在遥测文件中则不能使用。<br/>`<AdditionalType>Air+FixedWing</AdditionalType>` |
| Parent         | Parent hexadecimal object id. Useful to associate for example a missile (child object) and its launcher aircraft (parent object).<br/>父级十六进制对象ID。 例如，可用于关联导弹（子对象）及其发射飞机（父对象）。<br/> `Parent=2D50A7` |
| Next           | Hexadecimal id of the following object. Typically used to link waypoints together.<br/>跟随对象的十六进制ID。 通常用于将航路点链接在一起。<br/>`Next=40F1` |
| ShortName      | This abbreviated name will be displayed in the 3D view and in any other cases with small space to display the object name. Typically defined in Tacview database. Should not be defined in telemetry data. <br/>该缩写名称将显示在3D视图中，并且在任何其他情况下都将以较小的空间显示对象名称。 通常在Tacview数据库中定义。 不应在遥测数据中定义。<br/>`ShortName=A-10C` |
| LongName       | More detailed object name, used in small windows where there is more space than in a cluttered 3D view, but not enough space to display the full detailed name. For readability, it is suggested to start by the short name first (usually an abbreviation like the NATO code), followed by the object nickname / NATO name. Typically defined in Tacview database. Should not be defined in telemetry data. <br/>更详细的对象名称，用在比杂乱的3D视图有更多空间但空间不足以显示完整详细名称的小窗口中。 为了便于阅读，建议首先以短名称（通常是北约代码的缩写）开头，然后是对象昵称/北约名称。 通常在Tacview数据库中定义。 不应在遥测数据中定义。<br/>`LongName=A-10C Thunderbolt II` |
| FullName       | The full object name which is typically displayed in windows and other logs wherever there is enough space to display a lot of data without clutter issues. Typically defined in Tacview database. Should not be defined in telemetry data. <br/>完整的对象名称，通常在窗口和其他日志中显示，只要有足够的空间可以显示很多数据而不会出现混乱的情况。 通常在Tacview数据库中定义。 不应在遥测数据中定义。<br/>`FullName=Fairchild Republic A-10C Thunderbolt II` |
| CallSign       | The call sign will be displayed in priority over the object name and sometimes pilot name, especially in the 3D view and selection boxes. This is handy for mission debriefings where call signs are more informative than aircraft names. <br/>呼叫符号将优先于对象名称（有时是飞行员名称）显示，尤其是在3D视图和选择框中。 这对于任务报告很方便，在这些任务报告中，呼号比飞机名称更有意义。<br/>`CallSign=Jester` |
| Registration   | Aircraft registration (aka tail number)<br/>飞机注册号（又名尾号）<br/>`Registration=N594EX` |
| Squawk         | Current transponder code. Any code is possible, there is no limitation like with the old 4 digit transponders. <br/>当前的应答器代码。 任何代码都是可能的，没有像旧的4位数字应答器那样的限制。<br/>`Squawk=1200` |
| Pilot          | Aircraft pilot in command name. <br/>飞机驾驶员的指挥名称。<br/>`Pilot=Iceman` |
| Group          | Group the object belongs to. Used to group objects together. For example, a formation of F-16 flying a CAP together. <br/>对对象所属的组进行分组。 用于将对象分组在一起。 例如，一个F-16编队一起飞过CAP。<br/>Group=Springfield |
| Country        | [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code. <br/>国家代码<br/> `Country=us` |
| Coalition      | Coalition <br/>联军<br/>`Coalition=Allies`                   |
| Color          | Can be one of the following: Red, Orange, Green, Blue, Violet. Colors are predefined to ensure a clear display of the whole battlefield in all conditions. <br/>可以是以下颜色之一：红色，橙色，绿色，蓝色，紫色。 颜色是预定义的，以确保在所有情况下都能清晰显示整个战场。<br/>`Color=Blue` |
| Shape          | Filename of the 3D model which will be used to represent the object in the 3D view. 3D models must be in [Wavefront .obj file format](https://en.wikipedia.org/wiki/Wavefront_.obj_file) and stored in either `%ProgramData%\Tacview\Data\Meshes\` or `%APPDATA%\Tacview\Data\Meshes\`. Learn more about 3D models by reading the [dedicated documentation](https://www.tacview.net/documentation/3dobjects/en/) <br/>3D模型的文件名，将用于表示3D视图中的对象。 3D模型必须为Wavefront .obj文件格式，并存储在`%ProgramData%\Tacview\Data\Meshes\`或`%APPDATA%\Tacview\Data\Meshes\`中。可以阅读专用文档，了解有关3D模型的更多信息<br/>`Shape=Rotorcraft.Bell 206.obj` |
| Debug          | Debug text visible in the 3D view when Tacview is launched with the /Debug:on command line argument. <br/>使用/Debug:on命令行参数启动Tacview时，调试文本在3D视图中可见。<br/>`Debug=ObjectHandle:0x237CB9` |
| Label          | Free real-time text displayable in the 3D view and telemetry windows (to provide miscellaneous info to the end-user) <br/>可在3D视图和遥测窗口中显示的自由实时文本（向最终用户提供其他信息）<br/>`Label=Lead aircraft` |
| FocusedTarget  | Target currently focused by the object (typically used to designate laser beam target object, can also be used to show what the pilot is currently focused on)<br/>该对象当前瞄准的目标（通常用于指定激光束目标对象，也可用于显示飞行员当前瞄准的目标）<br/>`FocusedTarget=3001200` |
| LockedTarget   | Primary target hexadecimal id (could be locked using any device, like radar, IR, NVG, ...)<br/>首要目标十六进制ID（可以使用任何设备锁定，例如雷达，IR，NVG等）<br/>`LockedTarget=3001200` |

#### Numeric Properties

| Property Name                                                | Unit    | Meaning                                                      |
| :----------------------------------------------------------- | :------ | :----------------------------------------------------------- |
| Importance                                                   | ratio   | The higher the ratio, the more important is the object is (e.g. locally simulated aircraft could be 1.0 importance factor) Importance=1 |
| Slot                                                         | index   | Plane position in its Group (the lowest is the leader) Slot=0 |
| Disabled                                                     | boolean | Specifies that an object is disabled (typically out-of-combat) without being destroyed yet. This is particularly useful for combat training and shotlogs. Disabled=1 |
| Length                                                       | m       | Object length. Especially useful when displaying buildings. Length=20.5 |
| Width                                                        | m       | Object width. Especially useful when displaying buildings. Width=10.27 |
| Height                                                       | m       | Object height. Especially useful when displaying buildings. Height=4 |
| Radius                                                       | m       | Object bounding sphere radius. Object bounding sphere radius. Can be used to define custom explosion, smoke/grenade radius. Can be animated. Radius=82 |
| IAS                                                          | m/s     | Indicated airspeed IAS=69.4444                               |
| CAS                                                          | m/s     | Calibrated airspeed CAS=250                                  |
| TAS                                                          | m/s     | True airspeed TAS=75                                         |
| Mach                                                         | ratio   | Mach number Mach=0.75                                        |
| AOA                                                          | deg     | Angle of attack AOA=15.7                                     |
| AGL                                                          | m       | Object altitude above ground level AGL=1501.2                |
| HDG                                                          | deg     | Aircraft heading. When there is no roll and pitch data available, this property can be used to specify the yaw while keeping full rotation emulation in the 3D view. HDG=185.3 |
| HDM                                                          | deg     | Aircraft magnetic heading. Heading relative to local magnetic north. HDM=187.3 |
| Throttle                                                     | ratio   | Main/engine #1 throttle handle position (could be >1 for Afterburner and <0 for reverse) Throttle=0.75 |
| Afterburner                                                  | ratio   | Main/engine #1 afterburner status Afterburner=1              |
| AirBrakes                                                    | ratio   | Air brakes status AirBrakes=0                                |
| Flaps                                                        | ratio   | Flaps position Flaps=0.4                                     |
| LandingGear                                                  | ratio   | Landing gear status LandingGear=1                            |
| LandingGearHandle                                            | ratio   | Landing gear handle position LandingGearHandle=0             |
| Tailhook                                                     | ratio   | Arresting hook status Tailhook=1                             |
| Parachute                                                    | ratio   | Parachute status (not to be mistaken for DragChute) Parachute=0 |
| DragChute                                                    | ratio   | Drogue/Drag Parachute status DragChute=1                     |
| FuelWeight to FuelWeight9                                    | kg      | Fuel quantity currently available in each tanks (up to 10 tanks supported). FuelWeight4=8750 |
| FuelVolume to FuelVolume9                                    | l       | Fuel quantity currently available in each tanks (up to 10 tanks supported). FuelVolume=75 |
| FuelFlowWeight to FuelFlowWeight8                            | kg/hour | Fuel flow for each engine (up to 8 engines supported). FuelFlowWeight2=38.08 |
| FuelFlowVolume to FuelFlowVolume8                            | l/hour  | Fuel flow for each engine (up to 8 engines supported). FuelFlowVolume2=53.2 |
| RadarMode                                                    | number  | Radar mode (0 = off) RadarMode=1                             |
| RadarAzimuth                                                 | deg     | Radar azimuth (heading) relative to aircraft orientation RadarAzimuth=-20 |
| RadarElevation                                               | deg     | Radar elevation relative to aircraft orientation RadarElevation=15 |
| RadarRange                                                   | m       | Radar scan range RadarRange=296320                           |
| RadarHorizontalBeamwidth                                     | deg     | Radar beamwidth in azimuth RadarHorizontalBeamwidth=40       |
| RadarVerticalBeamwidth                                       | deg     | Radar beamwidth in elevation RadarVerticalBeamwidth=12       |
| LockedTargetMode                                             | number  | Primary target lock mode (0 = no lock/no target) LockedTargetMode=1 |
| LockedTargetAzimuth                                          | deg     | Primary target azimuth (heading) relative to aircraft orientation LockedTargetAzimuth=14.5 |
| LockedTargetElevation                                        | deg     | Primary target elevation relative to aircraft orientation LockedTargetElevation=0.9 |
| LockedTargetRange                                            | m       | Primary target distance to aircraft LockedTargetRange=17303  |
| EngagementMode EngagementMode2                               | number  | Enable/disable engagement range (such as when a SAM site turns off its radar) (0 = off) EngagementMode=1 |
| EngagementRange EngagementRange2 VerticalEngagementRange VerticalEngagementRange2 | m       | Engagement range for anti-aircraft units. This is the radius of the sphere which will be displayed in the 3D view. Typically used for SAM and AAA units, but this can be also relevant to warships. EngagementRange=2500 You can optionally specify the vertical engagement range to draw an ovoid engagement bubble. VerticalEngagementRange=1800 |
| RollControlInput PitchControlInput YawControlInput           | ratio   | Raw player HOTAS/Yoke position in real-life (flight sim input device) PitchControlInput=0.41 |
| RollControlPosition PitchControlPosition YawControlPosition  | ratio   | HOTAS/Yoke position in simulated (with response curves) or real-life cockpit PitchControlPosition=0.3 |
| RollTrimTab PitchTrimTab YawTrimTab                          | ratio   | Trim position for each axis PitchTrimTab=-0.15               |
| AileronLeft AileronRight Elevator Rudder                     | ratio   | Control surfaces position on the aircraft Elevator=0.15      |
| Visible                                                      | boolean | This flag is useful to hide specific objects from the 3D view. Can be used for a fog-of-war effect, or to prevent virtual objects from being displayed. Visible=0 |
| PilotHeadRoll PilotHeadPitch PilotHeadYaw                    | deg     | Pilot head orientation in the cockpit relative to the aircraft orientation PilotHeadPitch=12 |



#### Object Types (aka Tags)

Object types are now defined using a free combination of tags. The more tags, the more accurately an object is defined. Tags are separated by the plus sign +. Here are some examples:

| Object Kind      | Type (Tags)                               |
| :--------------- | :---------------------------------------- |
| Aircraft Carrier | Type=Heavy+Sea+Watercraft+AircraftCarrier |
| F-16C            | Type=Medium+Air+FixedWing                 |
| Bicycle          | Type=Light+Ground+Vehicle                 |
| AIM-120C         | Type=Medium+Weapon+Missile                |
| Waypoint         | Type=Navaid+Static+Waypoint               |

Here is the list of currently supported tags. Tacview will use them for display and analysis purposes.

| Use                                   | Tags                                                         |
| :------------------------------------ | :----------------------------------------------------------- |
| Class | Air<br>Ground<br>Sea<br>Weapon<br>Sensor<br>Navaid<br>Misc   |
| Attributes                            | Static<br>Heavy<br>Medium<br>Light<br>Minor                  |
| Basic Types                           | FixedWing<br>Rotorcraft<br>Armor<br>AntiAircraft<br>Vehicle<br>Watercraft<br>Human<br>Biologic<br>Missile<br>Rocket<br>Bomb<br>Torpedo<br>Projectile<br>Beam<br>Decoy<br>Building<br>Bullseye<br>Waypoint |
| Specific Types                        | Tank<br>Warship<br>Aircraft<br>Carrier<br>Submarine<br>Infantry<br>Parachutist<br>Shell<br>Bullet<br>Flare<br>Chaff<br>SmokeGrenade<br>Aerodrome<br>Container<br>Shrapnel<br>Explosion |



Here are the recommended common types (combination of tags) you should use to describe most of your objects for display in Tacview 1.x:

| Type             | Tags                                       |
| :--------------- | :----------------------------------------- |
| Plane            | Air + FixedWing                            |
| Helicopter       | Air + Rotorcraft                           |
| Anti-Aircraft    | Ground + AntiAircraft                      |
| Armor            | Ground + Heavy + Armor + Vehicle           |
| Tank             | Ground + Heavy + Armor + Vehicle + Tank    |
| Ground Vehicle   | Ground + Vehicle                           |
| Watercraft       | Sea + Watercraft                           |
| Warship          | Sea + Watercraft + Warship                 |
| Aircraft Carrier | Sea + Watercraft + AircraftCarrier         |
| Submarine        | Sea + Watercraft + Submarine               |
| Sonobuoy         | Sea + Sensor                               |
| Human            | Ground + Light + Human                     |
| Infantry         | Ground + Light + Human + Infantry          |
| Parachutist      | Ground + Light + Human + Air + Parachutist |
| Missile          | Weapon + Missile                           |
| Rocket           | Weapon + Rocket                            |
| Bomb             | Weapon + Bomb                              |
| Projectile       | Weapon + Projectile                        |
| Beam             | Weapon + Beam                              |
| Shell            | Projectile + Shell                         |
| Bullet           | Projectile + Bullet                        |
| Ballistic Shell  | Projectile + Shell + Heavy                 |
| Decoy            | Misc + Decoy                               |
| Flare            | Misc + Decoy + Flare                       |
| Chaff            | Misc + Decoy + Chaff                       |
| Smoke Grenade    | Misc + Decoy + SmokeGrenade                |
| Building         | Ground + Static + Building                 |
| Aerodrome        | Ground + Static + Aerodrome                |
| Bullseye         | Navaid + Static + Bullseye                 |
| Waypoint         | Navaid + Static + Waypoint                 |
| Container        | Misc + Container                           |
| Shrapnel         | Misc + Shrapnel                            |
| Minor Object     | Misc + Minor                               |
| Explosion        | Misc + Explosion                           |

### Comments

To help you during the debugging process of your exporter, it is possible to comment any line of the file by prefixing them with the double slash // like in C++.

```
// This line and the following are commented
// 3000102,T=41.6251307|41.5910417|2000.14,Name=C172
```

These lines will be ignored by Tacview when loading the file. Comments are not preserved. You will notice that they are discarded the next time you save the file from Tacview. If you want to include debug information which is preserved, you can use the dedicated Debug Event described earlier in the global properties.

Because of loading performance considerations, it is only possible to insert a comment at the beginning of a line.

```

```
